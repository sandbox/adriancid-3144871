<?php

/**
 * @file
 * Contains acme_sport.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function acme_sport_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help.
    case 'help.page.acme_sport':

      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module displays a dynamic list of NFL teams.');
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';

      $admin_route = [
        ':settings-page' => Url::fromRoute('acme_sport.admin_settings')->toString(),
      ];
      $nfl_team_route = [
        ':nfl-teams-page' => Url::fromRoute('acme_sport.nfl_teams')->toString(),
      ];

      $output .= '<dt>' . t('Configuring module settings') . '</dt>';
      $output .= '<dd>' . t('To configure the module settings visit the <a href=":settings-page">Settings</a> page, in the API Endpoint field enter the API endpoint URL with the NFL team data. To add the API Endpoint you need the <em>Administer ACME Sport</em> permission.', $admin_route) . '</dd>';
      $output .= '<dt>' . t('Available Pages') . '</dt>';
      $output .= '<dd>' . t('<a href=":nfl-teams-page">NFL Teams</a>. A page with all the NFL Teams', $nfl_team_route) . '</dd>';
      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function acme_sport_theme($existing, $type, $theme, $path) {
  return [
    'nfl_teams_cards' => [
      'variables' => [
        'module_path' => NULL,
        'nfc_title' => NULL,
        'nfc_teams' => NULL,
        'afc_title' => NULL,
        'afc_teams' => NULL,
      ],
      'template' => 'nfl-teams-cards',
    ],
    'nfl_teams_division' => [
      'variables' => [
        'module_path' => NULL,
        'teams_division' => NULL,
      ],
      'template' => 'nfl-teams-division',
    ],
  ];
}
