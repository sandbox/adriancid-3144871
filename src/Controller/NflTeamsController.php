<?php

namespace Drupal\acme_sport\Controller;

use Drupal\acme_sport\AcmeSportInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class NflTeamsController.
 */
class NflTeamsController extends ControllerBase {

  /**
   * The ACME Sport service.
   *
   * @var \Drupal\acme_sport\AcmeSportInterface
   */
  protected $acmeSport;

  /**
   * The module handler to execute the transliteration_overrides alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\acme_sport\AcmeSportInterface $acme_sport
   *   The ACME Sport service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(AcmeSportInterface $acme_sport, ModuleHandlerInterface $module_handler) {
    $this->acmeSport = $acme_sport;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('acme_sport'),
      $container->get('module_handler'),
    );
  }

  /**
   * Build the NFL Teams page.
   *
   * @return array
   *   A build array.
   */
  public function nflTeamsPage() {
    // Getting the module path.
    $module_path = $this->moduleHandler->getModule('acme_sport')->getPath();
    // Getting all the teams.
    $teams = $this->acmeSport->getTeamsByConference();
    // Getting the NFC Teams.
    $nfc_title = $this->t('NFC Teams');
    $nfc_teams = $teams['NFC'];
    // Getting the AFC Teams.
    $afc_title = $this->t('AFC Teams');
    $afc_teams = $teams['AFC'];

    $build['team_cards'] = [
      '#theme' => 'nfl_teams_cards',
      '#module_path' => $module_path,
      '#nfc_title' => $nfc_title,
      '#nfc_teams' => $nfc_teams,
      '#afc_title' => $afc_title,
      '#afc_teams' => $afc_teams,
    ];

    $teams_division = $this->acmeSport->getTeamsByConferenceAndDivision();

    $build['teams_division'] = [
      '#theme' => 'nfl_teams_division',
      '#module_path' => $module_path,
      '#teams_division' => $teams_division,
    ];

    return $build;
  }

}
