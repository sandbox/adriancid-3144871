<?php

namespace Drupal\acme_sport\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AcmeSportAdminSettings.
 */
class AcmeSportAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['acme_sport.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acme_sport_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_endpoint'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#size' => 100,
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('The API endpoint URL with the NFL team data'),
      '#default_value' => $this->config($this->getEditableConfigNames()[0])
        ->get('api_endpoint'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Getting the value from the form.
    $api_endpoint = $form_state->getValue('api_endpoint');
    // Saving the module configuration.
    $this->config($this->getEditableConfigNames()[0])
      ->set('api_endpoint', $api_endpoint)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
