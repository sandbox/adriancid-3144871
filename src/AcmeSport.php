<?php

namespace Drupal\acme_sport;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Component\Serialization\Json;
use Drupal\acme_sport\Utility\Acronym;

/**
 * Class AcmeSport.
 */
class AcmeSport implements AcmeSportInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The API Endpoint.
   *
   * @var string
   */
  protected $apiEndpoint;

  /**
   * The teams information.
   *
   * @var array
   */
  protected $teams;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->logger = $logger;
    // Getting the api endpoint.
    $this->apiEndpoint = $this->configFactory->get('acme_sport.settings')->get('api_endpoint');
    // Setting the teams array.
    $this->teams = $this->fetchData()['results']['data']['team'];
  }

  /**
   * Retrieves the information of the teams.
   *
   * @return array
   *   An array with the teams information.
   */
  protected function fetchData() {
    $data = '';
    try {
      $data = (string) $this->httpClient
        ->get($this->apiEndpoint, ['headers' => ['Accept' => 'application/json']])
        ->getBody();
    }
    catch (RequestException $exception) {
      $this->logger->get('acme_sport')->error($exception);
    }
    return Json::decode($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getTeamsByConference() {
    $teams = [];

    foreach ($this->teams as $team) {
      $conference = Acronym::generate($team['conference']);
      unset($team['conference']);
      $teams[$conference][] = $team;
    }
    return $teams;
  }

  /**
   * {@inheritdoc}
   */
  public function getTeamsByConferenceAndDivision() {
    $teams_by_conference = [];

    foreach ($this->getTeamsByConference() as $conference => $teams) {
      foreach ($teams as $team) {
        $division = $team['division'];
        unset($team['division']);
        $teams_by_conference[$conference . ' ' . $division][] = $team;
      }
    }
    ksort($teams_by_conference);
    return $teams_by_conference;
  }

}
