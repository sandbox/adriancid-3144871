<?php

namespace Drupal\acme_sport\Utility;

/**
 * Provides acronym helper methods.
 *
 * @ingroup utility
 */
class Acronym {

  /**
   * Generates an acronym from a string.
   *
   * @param string $string
   *   The string for which we will generate the acronym.
   *
   * @return string
   *   The acronym.
   */
  public static function generate($string) {
    return strtoupper(preg_replace('/\b(\w)|./', '$1', $string));
  }

}
