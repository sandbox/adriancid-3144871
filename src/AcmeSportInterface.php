<?php

namespace Drupal\acme_sport;

/**
 * Interface AcmeSportInterface.
 */
interface AcmeSportInterface {

  /**
   * Returns the teams by conference.
   *
   * @return array
   *   An array with all the teams keyed by conference.
   */
  public function getTeamsByConference();

  /**
   * Returns the teams by conference and division.
   *
   * @return array
   *   An array with all the teams keyed by conference and division.
   */
  public function getTeamsByConferenceAndDivision();

}
