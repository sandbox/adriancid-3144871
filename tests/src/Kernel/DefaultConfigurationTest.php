<?php

namespace Drupal\Tests\acme_sport\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the module configurations.
 *
 * @group acme_sport
 */
class DefaultConfigurationTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['acme_sport'];

  /**
   * Tests the default configuration values.
   */
  public function testDefaultConfigurationValues() {
    // Installing the configuration file.
    $this->installConfig(self::$modules);
    // Getting the config file.
    $config_file = $this->container->get('config.factory')->get('acme_sport.settings');
    $api_endpoint = 'http://delivery.chalk247.com/team_list/NFL.JSON?api_key=74db8efa2a6db279393b433d97c2bc843f8e32b0';
    // Checking if the onlyone_new_menu_entry variable is FALSE.
    $this->assertEquals($api_endpoint, $config_file->get('api_endpoint'), 'The default configuration value for the API endpoint is not correct.');
  }

}
