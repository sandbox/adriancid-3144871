<?php

namespace Drupal\Tests\acme_sport\Unit\Utility;

use Drupal\acme_sport\Utility\Acronym;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Acronym class methods.
 *
 * @group acme_sport
 * @coversDefaultClass \Drupal\acme_sport\Utility\Acronym
 */
class AcronymTest extends UnitTestCase {

  /**
   * Tests the generate() method.
   *
   * @param string $expected
   *   The expected result from calling the function.
   * @param string $string
   *   The string for which we will generate the acronym.
   *
   * @covers ::generate
   * @dataProvider providerGenerate
   */
  public function testGenerate($expected, $string) {
    // Testing the function.
    $this->assertEquals($expected, Acronym::generate($string));
  }

  /**
   * Data provider for testGenerate().
   *
   * @return array
   *   An array of arrays, each containing:
   *   - 'expected' - Expected return from Acronym::generate().
   *   - 'string' - The string for which we will generate the acronym.
   *
   * @see testGenerate()
   */
  public function providerGenerate() {
    $tests[] = ['TIATFY', 'This is a test for you'];
    $tests[] = ['CUB', 'Cuand Ubit Boir'];
    $tests[] = ['JASH', 'Just Another String Here'];
    $tests[] = ['WK', 'Was Kos'];
    $tests[] = ['L', 'Lops'];
    $tests[] = ['', ''];

    return $tests;
  }

}
