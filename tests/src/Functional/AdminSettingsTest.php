<?php

namespace Drupal\Tests\acme_sport\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the acme_sport_admin_settings configuration form.
 *
 * @group acme_sport
 */
class AdminSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['acme_sport'];

  /**
   * Tests the configuration form, the permission and the link.
   */
  public function testConfigurationForm() {
    // Going to the config page.
    $this->drupalGet('/admin/config/services/acme_sport/settings');

    // Checking that the page is not accesible for anonymous users.
    $this->assertSession()->statusCodeEquals(403);

    // Creating a user with the module permission.
    $account = $this->drupalCreateUser([
      'administer acme_sport',
      'access administration pages',
    ]);
    // Log in.
    $this->drupalLogin($account);

    // Checking the module link.
    $this->drupalGet('/admin/config/services');
    $this->assertSession()
      ->linkByHrefExists('/admin/config/services/acme_sport');

    // Going to the config page.
    $this->drupalGet('/admin/config/services/acme_sport/settings');
    // Checking that the request has succeeded.
    $this->assertSession()->statusCodeEquals(200);

    // Checking the page title.
    $this->assertSession()
      ->elementTextContains('css', 'h1', 'ACME Sport Settings');
    // Checking the field is not empty.
    $this->assertSession()->fieldValueNotEquals('api_endpoint', '');

    // Getting the config factory service.
    $config_factory = $this->container->get('config.factory');

    // Form value to send.
    $tests = [
      ['api_endpoint' => 'http://www.google.com'],
      ['api_endpoint' => 'http://www.drupal.org'],
    ];

    foreach ($tests as $edit) {
      // Sending the form.
      $this->drupalPostForm(NULL, $edit, 'op');
      // Verifiying the save message.
      $this->assertSession()
        ->pageTextContains('The configuration options have been saved.');
      // Getting variables.
      $api_endpoint = $config_factory->get('acme_sport.settings')
        ->get('api_endpoint');
      // Verifiying that the config values are stored.
      $this->assertEquals($edit['api_endpoint'], $api_endpoint, 'The configuration value for the api endpoint is not correct.');
    }
  }

}
